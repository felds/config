# ~/.bashrc: executed by bash(1) for non-login shells.

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=9999
HISTFILESIZE=99999
HISTCONTROL='ignorespace:ignoredups'

# some more ls aliases
alias ll='ls -AlFh'
alias la='ls -A'
alias lc='ls -CF'
alias gvim='mvim'
alias gvimdiff='mvimdiff'
alias quicklook='qlmanage -p'
alias lail='less -R+F'

#mysql bkp helper
function mysql_bkp_name {
    echo `date +%Y-%m-%d-%H-%M-%S`_${PWD##*/}.sql
}

# try until 0
function insist {
    until $@; do :; done
}

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# cutomizing prompt
source ~/.bash_prompt

# MacPorts Installer addition on 2011-02-11_at_02:52:26: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH

# Apache2
export PATH=/opt/local/apache2/bin:$PATH

# Mysql
export PATH=/usr/local/mysql/bin:$PATH
export PATH=/opt/local/lib/mysql55/bin:$PATH # MySQL via PORTS

# Ruby Gems
export PATH=$HOME/.gem/ruby/1.8/bin:$PATH

# My bins
export PATH=$HOME/bin:$PATH
export PATH=$HOME/.composer/vendor/bin:$PATH

# Colors on LS
export CLICOLOR=true
alias grep="grep --color=auto"

# Autocomplete after *
if [ "$PS1" ]; then
  complete -cf sudo
  complete -cf xargs
  complete -cf man
  complete -cf which
fi

# Autocomplete after git
source ~/.git_completition.sh

# Vagrant + PuPHPet + Digital Ocean
export SSL_CERT_FILE=/opt/local/share/curl/curl-ca-bundle.crt

# set default languages
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
