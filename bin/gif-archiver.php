#! /usr/bin/env php
<?php

// myself
$fullPath   = array_shift($argv);
$dir        = dirname($fullPath);
$script     = basename($fullPath);

// config
$config = json_decode(file_get_contents($dir . '/gif-archiver.json'), true);


// validation
if (count($argv) != 1) {
    die("
    Usage: {$script} input-file
    ");
}
$inputFile = array_shift($argv);

$targetFile =
    $config['target-folder'].
    DIRECTORY_SEPARATOR.
    uniqid(date('Y-m-d-')).
    '.gif';

rename($inputFile, $targetFile);
// echo "$inputFile → $targetFile" . PHP_EOL;
