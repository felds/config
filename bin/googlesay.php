#! /usr/bin/env php
<?php 

define('G_URL', 'http://translate.google.com/translate_tts');

$encode = 'UTF-8';
$lang   = 'en';
$q      = ''; 

// get params
$q = array();
foreach (array_slice($argv, 1) as $v)
{
    if (preg_match('/^--lang=(\w+)$/i', $v, $o))
    {
        $lang = $o[1];
        continue;
    }

    $q[] = $v;
}
$q = implode(' ', $q);

echo G_URL.'?'.http_build_query(array(
    'ie'  => $encode,
    'tl'  => $lang,
    'prev'=> 'input',
    'q'   => '',
));

for ($i = 0; $i < strlen($q); $i++)
    echo '%'.dechex(ord($q[$i]));
