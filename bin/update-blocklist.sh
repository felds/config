cd "$HOME/Library/Application Support/Transmission/blocklists"

echo "
    >> Clean all the things
    =======================
    "
rm -f *.{bin,txt}


lists="http://list.iblocklist.com/?list=bt_level1&fileformat=p2p&archiveformat=zip
       http://list.iblocklist.com/?list=bt_level2&fileformat=p2p&archiveformat=zip
       http://list.iblocklist.com/?list=bt_level3&fileformat=p2p&archiveformat=zip"


echo "
    >> Downloading lists
    ====================
    "

for list in $lists
do
    wget "$list" -O list.tmp
    unzip list.tmp
    rm list.tmp
done



echo "
    >> Removing comments
    ====================
    "

for txt in *.txt
do
    grep -v '^#' "$txt" | tee "$txt" > /dev/null
done



echo "
    =================
    =====  WIN  =====
    =================
    "
