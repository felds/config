"=============================="
"= START Vundle configuration ="
"=============================="

    set nocompatible              " be iMproved, required
    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin('~/.vundle/')
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')

    "-----------------"
    "- START plugins -"
    "-----------------"

        " let Vundle manage Vundle, required
        Plugin 'VundleVim/Vundle.vim'

        " Work with GIT from inside vim
        Plugin 'tpope/vim-fugitive'

        " Manipulate quotations marks
        Plugin 'tpope/vim-surround'

        " Automatic indentation detection
        Plugin 'tpope/vim-sleuth'

        " Makes the replay (..) behave correctly
        Plugin 'tpope/vim-repeat'

        " Awesome fuzzy file search
        Plugin 'kien/ctrlp.vim'

        " Standardize how some commands work
        Plugin 'tpope/vim-sensible'

        " Better ruler
        Plugin 'bling/vim-airline'

        " Whitespace highlight
        " Plugin 'jpalardy/spacehi.vim'

        " Automatically generate Ctags
        Plugin 'ludovicchabant/vim-gutentags'

        " Ctags viewer
        Plugin 'majutsushi/tagbar'

        " Use * to find visually selected blocks
        Plugin 'nelstrom/vim-visual-star-search'

        " Code completions and file formats
        Plugin 'lepture/vim-jinja'
        Plugin 'digitaltoad/vim-jade'
        Plugin 'othree/html5.vim'
        Plugin 'arnaud-lb/vim-php-namespace'
        Plugin 'vim-scripts/PHP-correct-Indenting'
        Plugin 'JulesWang/css.vim'
        Plugin 'pangloss/vim-javascript'
        Plugin 'mxw/vim-jsx'
        Plugin 'nginx.vim'
        Plugin 'editorconfig/editorconfig-vim'


        " Colorschemes
        Plugin 'flazz/vim-colorschemes'
        " Plugin 'felds/vim-colorschemes'

    "---------------"
    "- END plugins -"
    "---------------"

    " All of your Plugins must be added before the following line
    call vundle#end()            " required
    filetype plugin indent on    " required

"============================"
"= END Vundle configuration ="
"============================"

" Hide the current buffer when opening another one
" (e.g. when calling :Explore, :new, or using CtrlP)
set hidden

" Syntax highlight
syntax on

" Show status bar under the document
set ruler

" Show invisibles
set list

" Use filetype plugin
filetype plugin on
" Filetype associations
au BufRead,BufNewFile *.less set filetype=less
au BufRead,BufNewFile *.twig set filetype=jinja
au BufRead,BufNewFile *.es6 set filetype=javascript

" Highlighted search results
set hlsearch
" Use incremental searching
set incsearch


" force expandtab as default
set expandtab

" Don't let VIM try to fix the case when auto-completing
set infercase!

" Linewidth to infinite and beyond
set textwidth=0

" Do not wrap lines automatically
set nowrap

" Show line numbers by default
set number

" Jump 5 lines when running out of the screen
set scrolljump=5

" Indicate jump out of the screen when 3 lines before end of the screen
set scrolloff=5

" Map <CTRL>-B to run PHP parser check
noremap <C-B> :!php -l %<CR>

" Case insensitive search
set ignorecase

" Turn off audible bell
set visualbell

" Do not hide dotfiles (prevent vim-sensible from hiding them)
let g:netrw_list_hide = '\~$,^tags$,.sw[op]$,.netrwhist,.DS_Store'

" Use a single folder for all VIM temp files
" (it doesn't let VIM pollute the working dir)
let s:dir = has('win32') ? '$APPDATA/Vim' : match(system('uname'), "Darwin") > -1 ? '~/Library/Vim' : empty($XDG_DATA_HOME) ? '~/.local/share/vim' : '$XDG_DATA_HOME/vim'
if isdirectory(expand(s:dir))
    if &directory =~# '^\.,'
        let &directory = expand(s:dir) . '/swap//,' . &directory
    endif
    if &backupdir =~# '^\.,'
        let &backupdir = expand(s:dir) . '/backup//,' . &backupdir
    endif
    if exists('+undodir') && &undodir =~# '^\.\%(,\|$\)'
        let &undodir = expand(s:dir) . '/undo//,' . &undodir
    endif
endif
if exists('+undofile')
    set undofile
endif


"========================="
"= START Custom Commands ="
"========================="

    " Remove trailing whitespaces
    command! RMTrailings %s#\s\+$##

    " Indentation values
    " sw    = shiftwidth
    " sts   = softtabstop
    " ts    = tabstop
    command! -nargs=1 SetTabWidth set sw=<args> sts=<args> ts=<args> expandtab
    SetTabWidth 4


    command! -nargs=1 Fold setlocal foldmethod=indent foldlevel=<args>

"======================="
"= END Custom Commands ="
"======================="

"================="
"= START Visuals ="
"================="

    " open preview vertically
    let g:netrw_preview = 1

    colorscheme desert
    set bg=dark

    if has('gui_running')
        colorscheme atom

        if has('gui_gtk2') " gvim
            set guifont=Consolas\ 13
        else " everything else (mvim)
            set guifont=Source\ Code\ Pro:h13
        endif

        " Line height
        set linespace=3
    endif

"==============="
"= END Visuals ="
"==============="

"======================="
"= START Plugin Config ="
"======================="

    "-----------------"
    "- START Airline -"
    "-----------------"

        " @see https://github.com/bling/vim-airline/blob/master/doc/airline.txt
        let g:airline_powerline_fonts=0
        if !exists('g:airline_symbols')
            let g:airline_symbols = {}
        endif
        " unicode symbols
        let g:airline_left_sep = ''
        let g:airline_right_sep = ''
        let g:airline_symbols.linenr = 'L'
        let g:airline_symbols.branch = 'br:'

    "---------------"
    "- END Airline -"
    "---------------"

    "---------------"
    "- START CtrlP -"
    "---------------"

        " Ctrlp
        " let g:ctrlp_cmd = 'CtrlPMixed' " @see https://github.com/kien/ctrlp.vim/pull/420/files
        let g:ctrlp_show_hidden = 1 " @see https://github.com/kien/ctrlp.vim/issues/279
        let g:ctrlp_working_path_mode = 0 " @see https://github.com/kien/ctrlp.vim/issues/150
        let g:ctrlp_custom_ignore = 'node_modules\|vendor\|sass-cache\|DS_Store\|git' " @see https://github.com/kien/ctrlp.vim/issues/58
        let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . --cached --exclude-standard --others']

    "-------------"
    "- END CtrlP -"
    "-------------"

    "-----------------------"
    "- START PHP Namespace -"
    "-----------------------"

        function! IPhpInsertUse()
            call PhpInsertUse()
            call feedkeys('a',  'n')
        endfunction
        autocmd FileType php inoremap <Leader>u <Esc>:call IPhpInsertUse()<CR>
        autocmd FileType php noremap <Leader>u :call PhpInsertUse()<CR>

        autocmd FileType php inoremap <Leader>s <Esc>:call PhpSortUse()<CR>
        autocmd FileType php noremap <Leader>s :call PhpSortUse()<CR>

        autocmd FileType php inoremap <Leader>f <Esc>:call PhpExpandClass()<CR>
        autocmd FileType php noremap <Leader>f :call PhpExpandClass()<CR>

    "---------------------"
    "- END PHP Namespace -"
    "---------------------"

    "----------------"
    "- START Tagbar -"
    "----------------"

        nmap <F8> :TagbarToggle<CR>

    "--------------"
    "- END Tagbar -"
    "--------------"

    "----------------------"
    "- START Nginx syntax -"
    "----------------------"

        au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif 

    "--------------------"
    "- END Nginx syntax -"
    "--------------------"

    "----------------------"
    "- START Editorconfig -"
    "----------------------"

        set nofixendofline

    "--------------------"
    "- END Editorconfig -"
    "--------------------"

    "-------------------"
    "- START Gutentags -"
    "-------------------"

        let g:gutentags_define_advanced_commands = 1
        let g:gutentags_ctags_exclude = [ '*.css', '*.html', '*.js', '*.json', '*.xml',
                                  \ '*/tests/*', '*/*Test*',
                                  \ '*.phar', '*.ini', '*.rst', '*.md',
                                  \ '*vendor/*/test*', '*vendor/*/Test*',
                                  \ '*vendor/*/fixture*', '*vendor/*/Fixture*',
                                  \ '*var/cache*', '*var/log*' ]

    "-----------------"
    "- END Gutentags -"
    "-----------------"

"====================="
"= END Plugin Config ="
"====================="
